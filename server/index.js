const app = require('./lib/express')
const api = require('./api')
const config = require('./config/env')
const { dbConnect } = require('./lib/db')

// Importar os models
require('./models')

// Use routes express
app.use(api)

// Conectar ao mongo
if (config.database.dbAutoconnect) {
  dbConnect()
}