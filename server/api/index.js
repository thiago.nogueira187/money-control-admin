const api = require('express').Router()
const isEmpty = require('lodash/isEmpty')

const { seekAndImportFilesRecursive } = require('../lib/folder')

seekAndImportFilesRecursive(__dirname, router => {
  if (!isEmpty(router)) {
    api.use('/api', router)
  }
})

module.exports = api
