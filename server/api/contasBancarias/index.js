const express = require('express')
const router = express.Router()

router.get('/', function(req, res) {
  res.send('GET')
})

router.post('/', function(req, res) {
  res.send('POST')
})

router.put('/', function(req, res) {
  res.send('PUT')
})

router.delete('/', function(req, res) {
  res.send('DELETE')
})

module.exports = router