module.exports = {
  // The secret should be set to a non-guessable string that
  // is used to compute a session hash
  sessionSecret: 'MECJS',

  // conectar ao mongo automaticamente
  database: {
    dbAutoconnect: true,
    name: 'money_control',
    host: 'localhost',
    get fullPath () {
      return process.env.DB_CONNECTION || `mongodb://${this.host}/${this.name}`
    }
  },

  // The name of the MongoDB collection to store sessions in
  sessionCollection: 'sessions',

  http: {
    host: 'localhost',
    port: 4000
  }
}