/*
    load the config set on environment NODE_ENV
    /config/env/development.js
    /config/env/production.js
    /config/env/tests.js
 */

const merge = require('lodash/merge')
const all = require('./all')
const devSettings = require('./dev')
const prodSettings = require('./prod')
const testSettings = require('./test')

const envName = process.env.NODE_ENV || 'development'
let settings = {}

if (envName === 'development') {
  settings = merge(settings, all, devSettings)
} else if (envName === 'test') {
  settings = merge(settings, all, testSettings)
} else {
  settings = merge(settings, all, prodSettings)
}

module.exports = settings
