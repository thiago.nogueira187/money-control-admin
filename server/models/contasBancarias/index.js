/**
 *   @author     Thiago Nogueira
 *   @date       Ago 13 2018
 */
const mongoose = require('mongoose')

const COLECTION_NAME = 'contasBancarias'

const { ObjectId } = mongoose.Schema.Types

module.exports = createModel()

function createModel () {
  const ModelSchema = new mongoose.Schema({
    banco: {
      type: ObjectId,
      required: true,
      ref: 'bancos'
    },

    saldo: {
      type: Number,
      default: 0
    },

    usuario: {
      type: ObjectId,
      ref: 'usuarios'
    }
  }, { collection: COLECTION_NAME })

  return mongoose.model(COLECTION_NAME, ModelSchema)
}
