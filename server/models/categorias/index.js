/**
 *   @author     Thiago Nogueira
 *   @date       Ago 13 2018
 */
const mongoose = require('mongoose')
const { ObjectId } = mongoose.Schema.Types

const COLECTION_NAME = 'categorias'

module.exports = createModel()

function createModel () {
  const ModelSchema = new mongoose.Schema({
    nome: {
      type: String,
      required: true
    },

    ativo: {
      type: Boolean,
      default: true
    },

    subCategoria: {
      type: ObjectId,
      ref: 'categorias'
    }
  }, { collection: COLECTION_NAME })

  return mongoose.model(COLECTION_NAME, ModelSchema)
}
