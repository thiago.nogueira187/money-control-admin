/**
 *   @author     Thiago Nogueira
 *   @date       Ago 13 2018
 */
const mongoose = require('mongoose')

const COLECTION_NAME = 'usuarios'

module.exports = createModel()

function createModel () {
  const ModelSchema = new mongoose.Schema({
    nome: {
      type: String
    },

    login: {
      type: String
    },

    senha: {
      type: String
    },

    token: {
      type: String
    },

    ativo: {
      type: Boolean,
      default: true
    }
  }, { collection: COLECTION_NAME })

  return mongoose.model(COLECTION_NAME, ModelSchema)
}
