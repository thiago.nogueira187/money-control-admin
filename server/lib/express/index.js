'use strict'

// custom logger
const express = require('express')
const config = require('../../config/env')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()

const app = express()

// serves all static files in /public
app.use(express.static(`${__dirname}/../public`))

const HOST = process.env.HOST || config.http.host
const PORT = process.env.PORT || config.http.port

app.set('port', PORT)
app.use(jsonParser)
app.listen(PORT, HOST)

// boilerplate version
const version = `Money Control v${require('../../../../package.json').version}`

console.log(version)
console.info(`Server listening on http://${HOST}:${PORT}`)

module.exports = app