const fs = require('fs')
const path = require('path')

function seekAndImportFilesRecursive (currentDir, callback) {
  for (let child of fs.readdirSync(currentDir)) {
    const currentPath = path.join(currentDir, child)
    try {
      if (fs.statSync(currentPath).isDirectory()) {
        seekAndImportFilesRecursive(currentPath, callback)
      } else {
        // so incluir arquivos index.mjs. todas as rotas devem estar em arquivos index.mjs
        if (fs.statSync(currentPath).isFile() && child.includes('index.')) {
          const file = require(currentPath)

          if (typeof callback === 'function') {
            callback(file)
          }
        }
      }
    } catch (e) {
      // erro com o diretorio, igonorar e continuar
      console.error(e)
    }
  }
}

module.exports = {
  seekAndImportFilesRecursive
}