const connectMongo = require('connect-mongo')
const mongoose = require('mongoose')
const session = require('express-session')
const config = require('../../config/env')

const MongoStore = connectMongo(session)

let sessionMiddleware = session({
  name: 'session.id',
  secret: config.sessionSecret,
  resave: true,
  saveUninitialized: true,

  // using mongo to save session data
  store: new MongoStore({
    url: config.database.fullPath,
    collection: config.sessionCollection,
    autoReconnect: true
  })
})

function dbConnect () {
  mongoose.set('useCreateIndex', true)
  mongoose.connect(config.database.fullPath, { useNewUrlParser: true, useFindAndModify: false })
  mongoose.connection.on('open', openHandlerCalback)
  mongoose.connection.on('error', errorHandlerCallback)

  function openHandlerCalback () {
    console.info(`DB connected via Mongoose "${config.database.fullPath}"`)
  }

  function errorHandlerCallback (err) {
    console.error(`Fail to connect to DB "${onfig.database.fullPath}" Error: ${err.toString()}`)
  }

  return mongoose.connection
}

module.exports = { sessionMiddleware, dbConnect }